<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PlayersData;

class PlayersDataImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'players:data:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Players Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start');
        $this->info('');

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://fantasy.premierleague.com/api/bootstrap-static/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            // Set Here Your Requesred Headers
            'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $response = json_decode($response);

        $limit = 100;
        $counter = 0;
        foreach ($response->elements as $key => $value) {
            if ($counter >= $limit) {
                break;
            }

            if (PlayersData::where('id', $value->id)->count() == 0) {
                $player = PlayersData::create([
                    'id' => $value->id, 
                    'first_name' => $value->first_name, 
                    'last_name' => $value->second_name, 
                    'form' => number_format($value->form, 2), 
                    'total_points' => number_format($value->total_points, 2), 
                    'influence' => number_format($value->influence, 2), 
                    'creativity' => number_format($value->creativity, 2), 
                    'threat' => number_format($value->threat, 2), 
                    'ict_index' => number_format($value->ict_index, 2)
                ]);
                #dump($value->first_name);
                if ($player) {
                    $this->info('Inserted = ' . $value->id);
                } else {
                    $this->info('Fail = ' . $value->id);
                }
            } else {
                $this->info('Exists = ' . $value->id);
            }
            $counter++;
        }

        $this->info('');
        $this->info('End');
    }
}
