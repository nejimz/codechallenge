<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Players extends Model
{
    public $table = 'players';
    public $timestamps = false;
    protected $fillable = [
    	'id', 'full_name'
	];
}
