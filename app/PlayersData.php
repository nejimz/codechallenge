<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayersData extends Model
{
    public $table = 'players_data';
    public $timestamps = false;
    protected $fillable = [
    	'id', 'first_name', 'last_name', 'form', 'total_points', 'influence', 'creativity', 'threat', 'ict_index' 
	];
}
