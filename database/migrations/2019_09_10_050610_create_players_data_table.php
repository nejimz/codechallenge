<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players_data', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('first_name');
            $table->string('last_name');
            $table->decimal('form', 10, 2);
            $table->decimal('total_points', 10, 2);
            $table->decimal('influence', 10, 2);
            $table->decimal('creativity', 10, 2);
            $table->decimal('threat', 10, 2);
            $table->decimal('ict_index', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players_data');
    }
}
